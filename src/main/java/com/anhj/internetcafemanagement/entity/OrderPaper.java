package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.enums.OrderStatus;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperRequest;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperStatusUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderPaper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderPaperId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "foodId", nullable = false)
    private Food food;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private OrderStatus orderStatus;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putOrderPaperStatusData(OrderPaperStatusUpdateRequest statusUpdateRequest) {
        this.orderStatus = statusUpdateRequest.getOrderStatus();
        this.dateUpdate = LocalDateTime.now();
    }

    private OrderPaper(OrderPaperBuilder orderPaperBuilder) {
        this.food = orderPaperBuilder.food;
        this.orderStatus = orderPaperBuilder.orderStatus;
        this.quantity = orderPaperBuilder.quantity;
        this.dateCreate = orderPaperBuilder.dateCreate;
        this.dateUpdate = orderPaperBuilder.dateUpdate;
    }

    public static class OrderPaperBuilder implements CommonModelBuilder<OrderPaper> {
        private final Food food;
        private final OrderStatus orderStatus;
        private final Integer quantity;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public OrderPaperBuilder(Food food, OrderPaperRequest orderPaperRequest) {
            this.food = food;
            this.orderStatus = orderPaperRequest.getOrderStatus();
            this.quantity = orderPaperRequest.getQuantity();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public OrderPaper build() {
            return new OrderPaper(this);
        }
    }
}
