package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.enums.Categorize;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.foodModel.FoodRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long foodId;

    @Column(nullable = false, length = 254)
    private String foodImageUrl;

    @Column(nullable = false, length = 30)
    private String foodName;

    @Column(nullable = false)
    private BigDecimal foodPrice;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Categorize categorize;

    @Column(nullable = false)
    private Boolean isOnSale;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putFoodIsOnSaleUpdateFalse() {
        this.isOnSale = false;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putFoodIsOnSaleUpdateTrue() {
        this.isOnSale = true;
        this.dateUpdate = LocalDateTime.now();
    }

    private Food(FoodBuilder builder) {
        this.foodImageUrl = builder.foodImageUrl;
        this.foodName = builder.foodName;
        this.foodPrice = builder.foodPrice;
        this.categorize = builder.categorize;
        this.isOnSale = builder.isOnSale;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class FoodBuilder implements CommonModelBuilder<Food> {
        private final String foodImageUrl;
        private final String foodName;
        private final BigDecimal foodPrice;
        private final Categorize categorize;
        private final Boolean isOnSale;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public FoodBuilder(FoodRequest foodRequest) {
            this.foodImageUrl = foodRequest.getFoodImageUrl();
            this.foodName = foodRequest.getFoodName();
            this.foodPrice = foodRequest.getFoodPrice();
            this.categorize = foodRequest.getCategorize();
            this.isOnSale = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Food build() {
            return new Food(this);
        }
    }
}
