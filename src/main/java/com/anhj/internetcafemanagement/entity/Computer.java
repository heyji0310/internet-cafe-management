package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.computerModel.ComputerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usagePcRoomId", nullable = false)
    private UsagePcRoom usagePcRoom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderPaperId", nullable = false)
    private OrderPaper orderPaper;

    @Column(nullable = false, length = 10)
    private String seatNumber;

    @Column(nullable = false, length = 50)
    private String specification;

    private Computer(ComputerBuilder builder) {
        this.usagePcRoom = builder.usagePcRoom;
        this.orderPaper = builder.orderPaper;
        this.seatNumber = builder.seatNumber;
        this.specification = builder.specification;
    }

    public static class ComputerBuilder implements CommonModelBuilder<Computer> {
        private final UsagePcRoom usagePcRoom;
        private final OrderPaper orderPaper;
        private final String seatNumber;
        private final String specification;

        public ComputerBuilder(UsagePcRoom usagePcRoom, OrderPaper orderPaper, ComputerRequest request) {
            this.usagePcRoom = usagePcRoom;
            this.orderPaper = orderPaper;
            this.seatNumber = request.getSeatNumber();
            this.specification = request.getSpecification();
        }

        @Override
        public Computer build() {
            return new Computer(this);
        }
    }
}
