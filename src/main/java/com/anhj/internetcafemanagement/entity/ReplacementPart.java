package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.enums.PcState;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReplacementPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long replacementPartId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "computerId", nullable = false)
    private Computer computer;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private PcState pcState;

    @Column(nullable = false, length = 100)
    private String replacementPartName;

    @Column(nullable = false)
    private LocalDate dateReplacement;

    @Column(nullable = false)
    private BigDecimal costReplacement;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private ReplacementPart(ReplacementPartBuilder builder) {
        this.computer = builder.computer;
        this.pcState = builder.pcState;
        this.replacementPartName = builder.replacementPartName;
        this.dateReplacement = builder.dateReplacement;
        this.costReplacement = builder.costReplacement;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ReplacementPartBuilder implements CommonModelBuilder<ReplacementPart> {
        private final Computer computer;
        private final PcState pcState;
        private final String replacementPartName;
        private final LocalDate dateReplacement;
        private final BigDecimal costReplacement;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ReplacementPartBuilder(Computer computer, ReplacementPartRequest replacementRequest) {
            this.computer = computer;
            this.pcState = replacementRequest.getPcState();
            this.replacementPartName = replacementRequest.getReplacementPartName();
            this.dateReplacement = replacementRequest.getDateReplacement();
            this.costReplacement = replacementRequest.getCostReplacement();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public ReplacementPart build() {
            return new ReplacementPart(this);
        }
    }
}
