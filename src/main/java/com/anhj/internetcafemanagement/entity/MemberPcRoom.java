package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.enums.Gender;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberPcRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 15)
    private String memberPhone;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false, length = 30)
    private String loginId;

    @Column(nullable = false, length = 30)
    private String loginPw;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private MemberPcRoom(MemberPcRoomBuilder builder) {
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.gender = builder.gender;
        this.loginId = builder.loginId;
        this.loginPw = builder.loginPw;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MemberPcRoomBuilder implements CommonModelBuilder<MemberPcRoom> {
        private final String memberName;
        private final String memberPhone;
        private final Gender gender;
        private final String loginId;
        private final String loginPw;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MemberPcRoomBuilder(MemberPcRoomRequest request) {
            this.memberName = request.getMemberName();
            this.memberPhone = request.getMemberPhone();
            this.gender = request.getGender();
            this.loginId = request.getLoginId();
            this.loginPw = request.getLoginPw();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public MemberPcRoom build() {
            return new MemberPcRoom(this);
        }
    }
}
