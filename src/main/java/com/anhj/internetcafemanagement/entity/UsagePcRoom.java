package com.anhj.internetcafemanagement.entity;

import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import com.anhj.internetcafemanagement.model.usagePcRoomModel.UsagePcRoomStartRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsagePcRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long usagePcRoomId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private MemberPcRoom memberPcRoom;

    @Column(nullable = false)
    private Boolean isPcUsed;

    @Column(nullable = false)
    private LocalDateTime dateTimeStart;

    @Column(nullable = false)
    private LocalDateTime dateTimeEnd;

    @Column(nullable = false)
    private BigDecimal unitPrice;

    public void putUsagePcRoomEndData() {
        this.isPcUsed = false;
        this.dateTimeEnd = LocalDateTime.now();
    }

    private UsagePcRoom(UsagePcRoomStartBuilder startBuilder) {
        this.memberPcRoom = startBuilder.memberPcRoom;
        this.isPcUsed = startBuilder.isPcUsed;
        this.dateTimeStart = startBuilder.dateTimeStart;
        this.dateTimeEnd = startBuilder.dateTimeEnd;
        this.unitPrice = startBuilder.unitPrice;
    }

    public static class UsagePcRoomStartBuilder implements CommonModelBuilder<UsagePcRoom> {
        private final MemberPcRoom memberPcRoom;
        private final Boolean isPcUsed;
        private final LocalDateTime dateTimeStart;
        private final LocalDateTime dateTimeEnd;
        private final BigDecimal unitPrice;

        public UsagePcRoomStartBuilder(MemberPcRoom memberPcRoom, UsagePcRoomStartRequest usageStartRequest) {
            this.memberPcRoom = memberPcRoom;
            this.isPcUsed = true;
            this.dateTimeStart = LocalDateTime.now();
            this.dateTimeEnd = LocalDateTime.now();
            this.unitPrice = usageStartRequest.getUnitPrice();
        }

        @Override
        public UsagePcRoom build() {
            return new UsagePcRoom(this);
        }
    }
}
