package com.anhj.internetcafemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcState {
    AVAILABLE("사용가능")
    , DEFECTIVE("고장")
    , REPAIRING("수리중")
    ;

    private final String stateName;
}
