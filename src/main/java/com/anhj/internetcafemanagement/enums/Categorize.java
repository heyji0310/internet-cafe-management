package com.anhj.internetcafemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Categorize {
    RICE("밥류")
    , NOODLE("면류")
    , FRIED("튀김류")
    , BEVERAGE("음료")
    , COFFEE("커피")
    , SNACK("과자")
    , OTHER("기타")
    ;

    private final String categorizeName;
}
