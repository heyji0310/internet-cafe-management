package com.anhj.internetcafemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {
    RECEIVE("주문 접수")
    , COOKING("조리중")
    , DELIVERY("배달중")
    , COMPLETE("처리 완료")
    , CANCELLATION("주문 취소")
    ;

    private final String orderStatusName;
}
