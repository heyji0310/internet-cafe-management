package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.Food;
import com.anhj.internetcafemanagement.enums.Categorize;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food, Long> {
    List<Food> findByIsOnSaleTrueOrderByCategorizeAsc();

    List<Food> findAllByCategorizeOrderByFoodIdAsc(Categorize categorize);

    List<Food> findAllByFoodIdOrderByFoodIdAsc(Long foodId);
}
