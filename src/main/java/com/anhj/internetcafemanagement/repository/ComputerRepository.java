package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.Computer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComputerRepository extends JpaRepository<Computer, Long> {
    Computer findAllBySeatNumberOrderBySeatNumberAsc(String seatNumber);
}
