package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.UsagePcRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsagePcRoomRepository extends JpaRepository<UsagePcRoom, Long> {
    UsagePcRoom findAllByMemberPcRoom_LoginIdOrderByUsagePcRoomIdAsc(String loginId);
}
