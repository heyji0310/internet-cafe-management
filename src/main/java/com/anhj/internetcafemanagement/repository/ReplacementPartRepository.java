package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.ReplacementPart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplacementPartRepository extends JpaRepository<ReplacementPart, Long> {
}
