package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.OrderPaper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderPaperRepository extends JpaRepository<OrderPaper, Long> {
}
