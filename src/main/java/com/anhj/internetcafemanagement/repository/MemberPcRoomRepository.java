package com.anhj.internetcafemanagement.repository;

import com.anhj.internetcafemanagement.entity.MemberPcRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberPcRoomRepository extends JpaRepository<MemberPcRoom, Long> {
    MemberPcRoom findAllByLoginIdOrderByMemberNameAsc(String loginId);
}
