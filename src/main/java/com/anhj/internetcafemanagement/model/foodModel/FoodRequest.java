package com.anhj.internetcafemanagement.model.foodModel;

import com.anhj.internetcafemanagement.enums.Categorize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class FoodRequest {
    @ApiModelProperty(notes = "음식 이미지 url")
    @NotNull
    @Length(max = 254)
    private String foodImageUrl;

    @ApiModelProperty(notes = "음식 이름")
    @NotNull
    @Length(max = 30)
    private String foodName;

    @ApiModelProperty(notes = "음식 가격")
    @NotNull
    private BigDecimal foodPrice;

    @ApiModelProperty(notes = "음식 카테고리")
    @NotNull
    private Categorize categorize;
}
