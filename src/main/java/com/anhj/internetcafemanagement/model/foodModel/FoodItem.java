package com.anhj.internetcafemanagement.model.foodModel;

import com.anhj.internetcafemanagement.entity.Food;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodItem {
    @ApiModelProperty(notes = "음식메뉴 시퀀스")
    private Long foodId;

    @ApiModelProperty(notes = "음식메뉴 이미지 url")
    private String foodImageUrl;

    @ApiModelProperty(notes = "음식메뉴 전체정보([카테고리] 음식이름 + 가격)")
    private String foodFullInfo;

    @ApiModelProperty(notes = "음식메뉴 주문 가능 여부")
    private String isOnSale;

    private FoodItem(FoodItemBuilder foodItemBuilder) {
        this.foodId = foodItemBuilder.foodId;
        this.foodImageUrl = foodItemBuilder.foodImageUrl;
        this.foodFullInfo = foodItemBuilder.foodFullInfo;
        this.isOnSale = foodItemBuilder.isOnSale;
    }

    public static class FoodItemBuilder implements CommonModelBuilder<FoodItem> {
        private final Long foodId;
        private final String foodImageUrl;
        private final String foodFullInfo;
        private final String isOnSale;

        public FoodItemBuilder(Food food) {
            this.foodId = food.getFoodId();
            this.foodImageUrl = food.getFoodImageUrl();
            this.foodFullInfo = "[" + food.getCategorize().getCategorizeName() + "] " + food.getFoodName() + " " + food.getFoodPrice().intValue() + "원";
            this.isOnSale = food.getIsOnSale() ? "주문가능" : "재료소진";
        }

        @Override
        public FoodItem build() {
            return new FoodItem(this);
        }
    }
}
