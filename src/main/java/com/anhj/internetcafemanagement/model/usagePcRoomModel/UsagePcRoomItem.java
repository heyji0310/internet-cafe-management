package com.anhj.internetcafemanagement.model.usagePcRoomModel;

import com.anhj.internetcafemanagement.entity.UsagePcRoom;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsagePcRoomItem {
    @ApiModelProperty(notes = "이용내역 시퀀스")
    private Long usagePcRoomId;

    @ApiModelProperty(notes = "회원정보 (회원 이름 + 회원아이디)")
    private String memberNameIdInfo;

    @ApiModelProperty(notes = "PC 사용상태")
    private String isPcUsed;

    @ApiModelProperty(notes = "이용시간")
    private String usageTime;

    @ApiModelProperty(notes = "이용시간단가")
    private String usagePrice;

    @ApiModelProperty(notes = "이용시간금액")
    private String usageTimePrice;

    private UsagePcRoomItem(UsagePcRoomItemBuilder builder) {
        this.usagePcRoomId = builder.usagePcRoomId;
        this.memberNameIdInfo = builder.memberNameIdInfo;
        this.isPcUsed = builder.isPcUsed;
        this.usageTime = builder.usageTime;
        this.usagePrice = builder.usagePrice;
        this.usageTimePrice = builder.usageTimePrice;
    }

    public static class UsagePcRoomItemBuilder implements CommonModelBuilder<UsagePcRoomItem> {
        private final Long usagePcRoomId;
        private final String memberNameIdInfo;
        private final String isPcUsed;
        private final String usageTime;
        private final String usagePrice;
        private final String usageTimePrice;

        public UsagePcRoomItemBuilder(UsagePcRoom usagePcRoom) {
            this.usagePcRoomId = usagePcRoom.getUsagePcRoomId();
            this.memberNameIdInfo = usagePcRoom.getMemberPcRoom().getMemberName() + "(" + usagePcRoom.getMemberPcRoom().getGender().getGenderName() + ")";
            this.isPcUsed = usagePcRoom.getIsPcUsed() ? "사용중" : "사용완료";
            this.usageTime = (ChronoUnit.HOURS.between(usagePcRoom.getDateTimeStart(), usagePcRoom.getDateTimeEnd()) + "시간 " + ((ChronoUnit.MINUTES.between(usagePcRoom.getDateTimeStart(), usagePcRoom.getDateTimeEnd()))) / 60) + "분";
            this.usagePrice = usagePcRoom.getUnitPrice().intValue() + "원";
            this.usageTimePrice = ((ChronoUnit.HOURS.between(usagePcRoom.getDateTimeStart(), usagePcRoom.getDateTimeEnd())) * (usagePcRoom.getUnitPrice().intValue())) + ((ChronoUnit.MINUTES.between(usagePcRoom.getDateTimeStart(), usagePcRoom.getDateTimeEnd())) / 60 * (usagePcRoom.getUnitPrice().intValue() / 100)) + "원";
        }

        @Override
        public UsagePcRoomItem build() {
            return new UsagePcRoomItem(this);
        }
    }
}
