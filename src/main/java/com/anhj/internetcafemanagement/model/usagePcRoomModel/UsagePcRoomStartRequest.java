package com.anhj.internetcafemanagement.model.usagePcRoomModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class UsagePcRoomStartRequest {
    @ApiModelProperty(notes = "회원 Id", required = true)
    @NotNull
    private Long memberId;

    @ApiModelProperty(notes = "사용시간 단가", required = true)
    @NotNull
    private BigDecimal unitPrice;
}
