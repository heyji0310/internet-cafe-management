package com.anhj.internetcafemanagement.model.computerModel;

import com.anhj.internetcafemanagement.entity.Computer;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ComputerItem {
    @ApiModelProperty(notes = "컴퓨터 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "컴퓨터 좌석번호")
    private String seatNumber;

    @ApiModelProperty(notes = "컴퓨터 사양")
    private String specification;

    @ApiModelProperty(notes = "회원정보 (회원 이름 + 회원아이디)")
    private String memberNameIdInfo;

    @ApiModelProperty(notes = "PC 사용상태")
    private String isPcUsed;

    @ApiModelProperty(notes = "이용시간금액")
    private String usageTimePrice;

    @ApiModelProperty(notes = "[카테고리] + 음식이름")
    private String orderFood;

    @ApiModelProperty(notes = "음식주문상태")
    private String orderStatus;

    @ApiModelProperty(notes = "음식 주문 금액")
    private String orderPrice;

    @ApiModelProperty(notes = "전체 이용 금액")
    private String totalPrice;

    private ComputerItem(ComputerItemBuilder builder) {
        this.id = builder.id;
        this.seatNumber = builder.seatNumber;
        this.specification = builder.specification;
        this.memberNameIdInfo = builder.memberNameIdInfo;
        this.isPcUsed = builder.isPcUsed;
        this.usageTimePrice = builder.usageTimePrice;
        this.orderFood = builder.orderFood;
        this.orderStatus = builder.orderStatus;
        this.orderPrice = builder.orderPrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class ComputerItemBuilder implements CommonModelBuilder<ComputerItem> {
        private final Long id;
        private final String seatNumber;
        private final String specification;
        private final String memberNameIdInfo;
        private final String isPcUsed;
        private final String usageTimePrice;
        private final String orderFood;
        private final String orderStatus;
        private final String orderPrice;
        private final String totalPrice;

        public ComputerItemBuilder(Computer computer) {
            this.id = computer.getId();
            this.seatNumber = computer.getSeatNumber();
            this.specification = computer.getSpecification();
            this.memberNameIdInfo = computer.getUsagePcRoom().getMemberPcRoom().getMemberName() + "(" + computer.getUsagePcRoom().getMemberPcRoom().getLoginId() + ")";
            this.isPcUsed = computer.getUsagePcRoom().getIsPcUsed() ? "사용중" : "사용완료";
            this.usageTimePrice = ((ChronoUnit.HOURS.between(computer.getUsagePcRoom().getDateTimeStart(), computer.getUsagePcRoom().getDateTimeEnd())) * (computer.getUsagePcRoom().getUnitPrice().intValue())) + ((ChronoUnit.MINUTES.between(computer.getUsagePcRoom().getDateTimeStart(), computer.getUsagePcRoom().getDateTimeEnd())) / 60 * (computer.getUsagePcRoom().getUnitPrice().intValue() / 100)) + "원";
            this.orderFood = "[" + computer.getOrderPaper().getFood().getCategorize().getCategorizeName() + "] " + computer.getOrderPaper().getFood().getFoodName();
            this.orderStatus = computer.getOrderPaper().getOrderStatus().getOrderStatusName();
            this.orderPrice = (computer.getOrderPaper().getFood().getFoodPrice().intValue() * computer.getOrderPaper().getQuantity()) + "원";
            this.totalPrice = ((ChronoUnit.HOURS.between(computer.getUsagePcRoom().getDateTimeStart(), computer.getUsagePcRoom().getDateTimeEnd())) * (computer.getUsagePcRoom().getUnitPrice().intValue())) + ((ChronoUnit.MINUTES.between(computer.getUsagePcRoom().getDateTimeStart(), computer.getUsagePcRoom().getDateTimeEnd())) / 60 * (computer.getUsagePcRoom().getUnitPrice().intValue() / 100)) + (computer.getOrderPaper().getFood().getFoodPrice().intValue() * computer.getOrderPaper().getQuantity()) + "원";
        }

        @Override
        public ComputerItem build() {
            return new ComputerItem(this);
        }
    }
}
