package com.anhj.internetcafemanagement.model.computerModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ComputerRequest {
    @ApiModelProperty(notes = "PC 사용내역 id")
    @NotNull
    private Long usagePcRoomId;

    @ApiModelProperty(notes = "음식 주문내역 id")
    @NotNull
    private Long orderPaperId;

    @ApiModelProperty(notes = "좌석번호")
    @NotNull
    @Length(min = 2, max = 10)
    private String seatNumber;

    @ApiModelProperty(notes = "PC 사양")
    @NotNull
    @Length(max = 50)
    private String specification;
}
