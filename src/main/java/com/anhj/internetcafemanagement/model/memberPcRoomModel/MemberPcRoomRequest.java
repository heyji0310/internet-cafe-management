package com.anhj.internetcafemanagement.model.memberPcRoomModel;

import com.anhj.internetcafemanagement.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberPcRoomRequest {
    @ApiModelProperty(notes = "회원 이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(notes = "회원 연락처")
    @NotNull
    @Length(min = 12, max = 15)
    private String memberPhone;

    @ApiModelProperty(notes = "회원 성별")
    @NotNull
    private Gender gender;

    @ApiModelProperty(notes = "Log-in Id")
    @NotNull
    @Length(min = 3, max = 30)
    private String loginId;

    @ApiModelProperty(notes = "Log-in Password")
    @NotNull
    @Length(min = 3, max = 30)
    private String loginPw;
}
