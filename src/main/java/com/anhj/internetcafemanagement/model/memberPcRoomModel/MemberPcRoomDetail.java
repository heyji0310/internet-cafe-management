package com.anhj.internetcafemanagement.model.memberPcRoomModel;

import com.anhj.internetcafemanagement.entity.MemberPcRoom;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberPcRoomDetail {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 정보 - 이름(성별)")
    private String memberFullInfo;

    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;

    @ApiModelProperty(notes = "로그인 아이디")
    private String loginId;

    private MemberPcRoomDetail(MemberPcRoomDetailBuilder detailBuilder) {
        this.memberId = detailBuilder.memberId;
        this.memberFullInfo = detailBuilder.memberFullInfo;
        this.memberPhone = detailBuilder.memberPhone;
        this.loginId = detailBuilder.loginId;
    }

    public static class MemberPcRoomDetailBuilder implements CommonModelBuilder<MemberPcRoomDetail> {
        private final Long memberId;
        private final String memberFullInfo;
        private final String memberPhone;
        private final String loginId;

        public MemberPcRoomDetailBuilder(MemberPcRoom memberPcRoom) {
            this.memberId = memberPcRoom.getMemberId();
            this.memberFullInfo = memberPcRoom.getMemberName() + "(" + memberPcRoom.getGender().getGenderName() + ")";
            this.memberPhone = memberPcRoom.getMemberPhone();
            this.loginId = memberPcRoom.getLoginId();
        }

        @Override
        public MemberPcRoomDetail build() {
            return new MemberPcRoomDetail(this);
        }
    }
}
