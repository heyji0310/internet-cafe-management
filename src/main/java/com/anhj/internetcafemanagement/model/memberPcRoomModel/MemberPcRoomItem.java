package com.anhj.internetcafemanagement.model.memberPcRoomModel;

import com.anhj.internetcafemanagement.entity.MemberPcRoom;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberPcRoomItem {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 정보 - 이름(성별)")
    private String memberInfo;

    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;

    @ApiModelProperty(notes = "회원 로그인 정보(id/pw)")
    private String loginInfo;

    private MemberPcRoomItem(MemberPcRoomItemBuilder builder) {
        this.memberId = builder.memberId;
        this.memberInfo = builder.memberInfo;
        this.memberPhone = builder.memberPhone;
        this.loginInfo = builder.loginInfo;
    }

    public static class MemberPcRoomItemBuilder implements CommonModelBuilder<MemberPcRoomItem> {
        private final Long memberId;
        private final String memberInfo;
        private final String memberPhone;
        private final String loginInfo;

        public MemberPcRoomItemBuilder(MemberPcRoom memberPcRoom) {
            this.memberId = memberPcRoom.getMemberId();
            this.memberInfo = memberPcRoom.getMemberName() + "(" + memberPcRoom.getGender().getGenderName() + ")";
            this.memberPhone = memberPcRoom.getMemberPhone();
            this.loginInfo = "ID: " + memberPcRoom.getLoginId() + " / PW: " + memberPcRoom.getLoginPw();
        }

        @Override
        public MemberPcRoomItem build() {
            return new MemberPcRoomItem(this);
        }
    }
}
