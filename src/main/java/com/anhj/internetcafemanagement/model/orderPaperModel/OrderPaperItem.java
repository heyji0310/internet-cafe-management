package com.anhj.internetcafemanagement.model.orderPaperModel;

import com.anhj.internetcafemanagement.entity.OrderPaper;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderPaperItem {
    @ApiModelProperty(notes = "주문내역 시퀀스")
    private Long orderPaperId;

    @ApiModelProperty(notes = "음식메뉴 정보 - [카테고리] + 메뉴이름")
    private String menuInfo;

    @ApiModelProperty(notes = "주문상태")
    private String orderStatus;

    @ApiModelProperty(notes = "주문 수량")
    private String quantity;

    private OrderPaperItem(OrderPaperItemBuilder builder) {
        this.orderPaperId = builder.orderPaperId;
        this.menuInfo = builder.menuInfo;
        this.orderStatus = builder.orderStatus;
        this.quantity = builder.quantity;
    }

    public static class OrderPaperItemBuilder implements CommonModelBuilder<OrderPaperItem> {
        private final Long orderPaperId;
        private final String menuInfo;
        private final String orderStatus;
        private final String quantity;

        public OrderPaperItemBuilder(OrderPaper orderPaper) {
            this.orderPaperId = orderPaper.getOrderPaperId();
            this.menuInfo = "[" + orderPaper.getFood().getCategorize().getCategorizeName() + "] " + orderPaper.getFood().getFoodName();
            this.orderStatus = orderPaper.getOrderStatus().getOrderStatusName();
            this.quantity = orderPaper.getQuantity() + "개";
        }

        @Override
        public OrderPaperItem build() {
            return new OrderPaperItem(this);
        }
    }
}
