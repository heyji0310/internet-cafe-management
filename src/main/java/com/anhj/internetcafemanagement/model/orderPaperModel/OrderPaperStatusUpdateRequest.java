package com.anhj.internetcafemanagement.model.orderPaperModel;

import com.anhj.internetcafemanagement.enums.OrderStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderPaperStatusUpdateRequest {
    @ApiModelProperty(notes = "음식 주문 상태")
    private OrderStatus orderStatus;
}
