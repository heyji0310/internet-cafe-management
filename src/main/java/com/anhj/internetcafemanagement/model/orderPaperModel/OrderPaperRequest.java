package com.anhj.internetcafemanagement.model.orderPaperModel;

import com.anhj.internetcafemanagement.enums.OrderStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OrderPaperRequest {
    @ApiModelProperty(notes = "음식 메뉴 id", required = true)
    @NotNull
    private Long foodId;

    @ApiModelProperty(notes = "음식 주문 상태", required = true)
    @NotNull
    private OrderStatus orderStatus;

    @ApiModelProperty(notes = "음식 주문 수량", required = true)
    @NotNull
    private Integer quantity;
}
