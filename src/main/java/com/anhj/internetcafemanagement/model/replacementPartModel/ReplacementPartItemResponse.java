package com.anhj.internetcafemanagement.model.replacementPartModel;

import com.anhj.internetcafemanagement.entity.ReplacementPart;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReplacementPartItemResponse {
    @ApiModelProperty(notes = "교체내역 시퀀스")
    private Long replacementPartId;

    @ApiModelProperty(notes = "좌석번호")
    private String seatNumber;

    @ApiModelProperty(notes = "사용한 회원 정보([회원 아이디] - 회원이름(성별)")
    private String usageMemberFullInfo;

    @ApiModelProperty(notes = "PC 이용내역 정보([PC 사용상태] + 이용시간 / 단가)")
    private String usagePcRoomFullInfo;

    @ApiModelProperty(notes = "PC 이용내역 금액")
    private String usageTotalPrice;

    @ApiModelProperty(notes = "주문내역 정보([카테고리] + 메뉴이름 / 가격 / 주문수량)")
    private String orderPaperFullInfo;

    @ApiModelProperty(notes = "주문내역 금액")
    private String orderPaperTotalPrice;

    @ApiModelProperty(notes = "총 이용내역 금액")
    private String totalPrice;

    @ApiModelProperty(notes = "부품교체내역 정보([부품교체일] + 부품교체명 / 비용)")
    private String replacementFullInfo;



    private ReplacementPartItemResponse(ReplacementPartItemResponseBuilder builder) {
        this.replacementPartId = builder.replacementPartId;
        this.seatNumber = builder.seatNumber;
        this.usageMemberFullInfo = builder.usageMemberFullInfo;
        this.usagePcRoomFullInfo = builder.usagePcRoomFullInfo;
        this.usageTotalPrice = builder.usageTotalPrice;
        this.orderPaperFullInfo = builder.orderPaperFullInfo;
        this.orderPaperTotalPrice = builder.orderPaperTotalPrice;
        this.totalPrice = builder.totalPrice;
        this.replacementFullInfo = builder.replacementFullInfo;
    }

    public static class ReplacementPartItemResponseBuilder implements CommonModelBuilder<ReplacementPartItemResponse> {
        private final Long replacementPartId;
        private final String seatNumber;
        private final String usageMemberFullInfo;
        private final String usagePcRoomFullInfo;
        private final String usageTotalPrice;
        private final String orderPaperFullInfo;
        private final String orderPaperTotalPrice;
        private final String totalPrice;
        private final String replacementFullInfo;

        public ReplacementPartItemResponseBuilder(ReplacementPart replacementPart) {
            this.replacementPartId = replacementPart.getReplacementPartId();
            this.seatNumber = replacementPart.getComputer().getSeatNumber();

            String loginID = replacementPart.getComputer().getUsagePcRoom().getMemberPcRoom().getLoginId();
            String memberName = replacementPart.getComputer().getUsagePcRoom().getMemberPcRoom().getMemberName();
            String gender = replacementPart.getComputer().getUsagePcRoom().getMemberPcRoom().getGender().getGenderName();
            this.usageMemberFullInfo = "[" + loginID + "] - " + memberName + "(" + gender + ")";

            String isPcUsed = replacementPart.getComputer().getUsagePcRoom().getIsPcUsed() ? "사용중" : "사용완료";

            long usageHoursTime = ChronoUnit.HOURS.between(replacementPart.getComputer().getUsagePcRoom().getDateTimeStart(), replacementPart.getComputer().getUsagePcRoom().getDateTimeEnd());
            long usageMinutesTime = (ChronoUnit.MINUTES.between(replacementPart.getComputer().getUsagePcRoom().getDateTimeStart(), replacementPart.getComputer().getUsagePcRoom().getDateTimeEnd())) / 60;
            long unitPrice = replacementPart.getComputer().getUsagePcRoom().getUnitPrice().intValue();

            this.usagePcRoomFullInfo = "[" + isPcUsed + "] " + usageHoursTime + "시간 " + usageMinutesTime + "분 / " + unitPrice + "원";
            this.usageTotalPrice = ((usageHoursTime) * (unitPrice)) + ((usageMinutesTime) * (unitPrice / 100)) + "원";

            String foodCategorize = replacementPart.getComputer().getOrderPaper().getFood().getCategorize().getCategorizeName();
            String foodName = replacementPart.getComputer().getOrderPaper().getFood().getFoodName();
            long foodPrice = replacementPart.getComputer().getOrderPaper().getFood().getFoodPrice().intValue();
            Integer orderQuantity = replacementPart.getComputer().getOrderPaper().getQuantity();
            this.orderPaperFullInfo = "[" + foodCategorize + "] " + foodName + " / " + foodPrice + "원, " + orderQuantity + "개";
            this.orderPaperTotalPrice = ((foodPrice) * (orderQuantity)) + "원";

            this.totalPrice = ((usageHoursTime * unitPrice) + (usageMinutesTime * (unitPrice / 100))) + (foodPrice * orderQuantity) + "원";

            String dateReplacement = replacementPart.getDateReplacement().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
            String replacementName = replacementPart.getReplacementPartName();
            long costReplacement = replacementPart.getCostReplacement().intValue();
            this.replacementFullInfo = "[" + dateReplacement + "] " + replacementName + " / " + costReplacement / 10000 + "만원";
        }

        @Override
        public ReplacementPartItemResponse build() {
            return new ReplacementPartItemResponse(this);
        }
    }
}
