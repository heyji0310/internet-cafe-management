package com.anhj.internetcafemanagement.model.replacementPartModel;

import com.anhj.internetcafemanagement.enums.PcState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class ReplacementPartRequest {
    @ApiModelProperty(notes = "컴퓨터 Id", required = true)
    @NotNull
    private Long computerId;

    @ApiModelProperty(notes = "PC 상태", required = true)
    @NotNull
    private PcState pcState;

    @ApiModelProperty(notes = "부품 교체명", required = true)
    @NotNull
    private String replacementPartName;

    @ApiModelProperty(notes = "부품 교체일", required = true)
    @NotNull
    private LocalDate dateReplacement;

    @ApiModelProperty(notes = "부품 교체비용", required = true)
    @NotNull
    private BigDecimal costReplacement;
}
