package com.anhj.internetcafemanagement.model.replacementPartModel;

import com.anhj.internetcafemanagement.entity.ReplacementPart;
import com.anhj.internetcafemanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReplacementPartDetail {
    @ApiModelProperty(notes = "교체내역 시퀀스")
    private Long replacementPartId;
    @ApiModelProperty(notes = "컴퓨터 시퀀스")
    private Long computerId;

    @ApiModelProperty(notes = "PC 정보 [PC 상태] + 부품교체명 / 비용")
    private String pcState;

    @ApiModelProperty(notes = "부품 교체일(yyyy년 MM월 dd일)")
    private String dateReplacement;

    private ReplacementPartDetail(ReplacementPartDetailBuilder builder) {
        this.replacementPartId = builder.replacementPartId;
        this.computerId = builder.computerId;
        this.pcState = builder.pcState;
        this.dateReplacement = builder.dateReplacement;
    }

    public static class ReplacementPartDetailBuilder implements CommonModelBuilder<ReplacementPartDetail> {
        private final Long replacementPartId;
        private final Long computerId;
        private final String pcState;
        private final String dateReplacement;

        public ReplacementPartDetailBuilder(ReplacementPart replacementPart) {
            this.replacementPartId = replacementPart.getReplacementPartId();
            this.computerId = replacementPart.getComputer().getId();
            this.pcState = "[" + replacementPart.getPcState().getStateName() + "] " + replacementPart.getReplacementPartName() + " / " + replacementPart.getCostReplacement().intValue() + "원";
            this.dateReplacement = replacementPart.getDateReplacement().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일"));
        }

        @Override
        public ReplacementPartDetail build() {
            return new ReplacementPartDetail(this);
        }
    }
}
