package com.anhj.internetcafemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternetCafeManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(InternetCafeManagementApplication.class, args);
	}

}
