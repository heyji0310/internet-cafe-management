package com.anhj.internetcafemanagement.service;

import com.anhj.internetcafemanagement.entity.Computer;
import com.anhj.internetcafemanagement.entity.ReplacementPart;
import com.anhj.internetcafemanagement.exception.CMissingDataException;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartDetail;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartItemResponse;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartRequest;
import com.anhj.internetcafemanagement.repository.ReplacementPartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReplacementPartService {
    private final ReplacementPartRepository replacementPartRepository;

    public void setReplacementPart(Computer computer, ReplacementPartRequest request) {
        ReplacementPart replacementPart = new ReplacementPart.ReplacementPartBuilder(computer, request).build();
        replacementPartRepository.save(replacementPart);
    }

    public ReplacementPartDetail getReplacement(long replacementId) {
        ReplacementPart replacementPart = replacementPartRepository.findById(replacementId).orElseThrow(CMissingDataException::new);
        return new ReplacementPartDetail.ReplacementPartDetailBuilder(replacementPart).build();
    }

    public ReplacementPartItemResponse getResponse(long id) {
        ReplacementPart replacementPart = replacementPartRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ReplacementPartItemResponse.ReplacementPartItemResponseBuilder(replacementPart).build();
    }
}
