package com.anhj.internetcafemanagement.service;

import com.anhj.internetcafemanagement.entity.Food;
import com.anhj.internetcafemanagement.entity.OrderPaper;
import com.anhj.internetcafemanagement.enums.Categorize;
import com.anhj.internetcafemanagement.exception.CMissingDataException;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.foodModel.FoodItem;
import com.anhj.internetcafemanagement.model.foodModel.FoodRequest;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperItem;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperRequest;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperStatusUpdateRequest;
import com.anhj.internetcafemanagement.repository.FoodRepository;
import com.anhj.internetcafemanagement.repository.OrderPaperRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodOrderPaperService {
    private final FoodRepository foodRepository;
    private final OrderPaperRepository orderPaperRepository;

    public OrderPaper getOrderPaperData(long orderPaperId) {
        return orderPaperRepository.findById(orderPaperId).orElseThrow(CMissingDataException::new);
    }

    public Food getFoodData(long id) {
        return foodRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    // 음식(메뉴) 등록
    public void setFood(FoodRequest foodRequest) {
        Food food = new Food.FoodBuilder(foodRequest).build();
        foodRepository.save(food);
    }

    // 음식 주문내역 등록
    public void setOrderPaper(Food food, OrderPaperRequest orderPaperBuilder) {
        OrderPaper orderPaper = new OrderPaper.OrderPaperBuilder(food, orderPaperBuilder).build();
        orderPaperRepository.save(orderPaper);
    }

    // 판매 가능 음식메뉴 전체 조회
    public ListResult<FoodItem> getFoods() {
        List<Food> foods = foodRepository.findByIsOnSaleTrueOrderByCategorizeAsc();
        List<FoodItem> result = new LinkedList<>();
        foods.forEach(food -> {
            FoodItem addItem = new FoodItem.FoodItemBuilder(food).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    // 카테고리별 음식메뉴 조회
    public ListResult<FoodItem> getFoods(Categorize categorize) {
        List<Food> foods = foodRepository.findAllByCategorizeOrderByFoodIdAsc(categorize);
        List<FoodItem> result = new LinkedList<>();
        foods.forEach(food -> {
            FoodItem addItem = new FoodItem.FoodItemBuilder(food).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    // 음식 "재료소진"으로 수정
    public void putFoodIsOnSaleUpdateFalse(long id) {
        Food food = foodRepository.findById(id).orElseThrow(CMissingDataException::new);
        food.putFoodIsOnSaleUpdateFalse();
        foodRepository.save(food);
    }

    // 음식 "주문가능"으로 수정
    public void putFoodIsOnSaleUpdateTrue(long id) {
        Food food = foodRepository.findById(id).orElseThrow(CMissingDataException::new);
        food.putFoodIsOnSaleUpdateTrue();
        foodRepository.save(food);
    }

    // 주문내역 전체조회
    public ListResult<OrderPaperItem> getOrderPapers() {
        List<OrderPaper> orderPapers = orderPaperRepository.findAll();
        List<OrderPaperItem> result = new LinkedList<>();

        orderPapers.forEach(orderPaper -> {
            OrderPaperItem addItem = new OrderPaperItem.OrderPaperItemBuilder(orderPaper).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    // 주문상태 수정
    public void putOrderPaperStatus(long orderPaperId, OrderPaperStatusUpdateRequest statusUpdateRequest) {
        OrderPaper orderPaper = orderPaperRepository.findById(orderPaperId).orElseThrow(CMissingDataException::new);
        orderPaper.putOrderPaperStatusData(statusUpdateRequest);
        orderPaperRepository.save(orderPaper);
    }
}
