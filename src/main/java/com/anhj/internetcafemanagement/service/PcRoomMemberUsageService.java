package com.anhj.internetcafemanagement.service;

import com.anhj.internetcafemanagement.entity.MemberPcRoom;
import com.anhj.internetcafemanagement.entity.UsagePcRoom;
import com.anhj.internetcafemanagement.exception.CMissingDataException;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomDetail;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomItem;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomRequest;
import com.anhj.internetcafemanagement.model.usagePcRoomModel.UsagePcRoomItem;
import com.anhj.internetcafemanagement.model.usagePcRoomModel.UsagePcRoomStartRequest;
import com.anhj.internetcafemanagement.repository.MemberPcRoomRepository;
import com.anhj.internetcafemanagement.repository.UsagePcRoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcRoomMemberUsageService {
    private final MemberPcRoomRepository memberPcRoomRepository;
    private final UsagePcRoomRepository usagePcRoomRepository;

    public UsagePcRoom getUsagePcRoomData(long usagePCRoomId) {
        return usagePcRoomRepository.findById(usagePCRoomId).orElseThrow(CMissingDataException::new);
    }

    public MemberPcRoom getMemberData(long id) {
        return memberPcRoomRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    // PC방 회원 등록
    public void setMemberPcRoom(MemberPcRoomRequest memberRequest) {
        MemberPcRoom memberPcRoom = new MemberPcRoom.MemberPcRoomBuilder(memberRequest).build();
        memberPcRoomRepository.save(memberPcRoom);
    }

    // PC방 사용시작 등록(log-in)
    public void setUsagePcRoomStart(MemberPcRoom memberPcRoom, UsagePcRoomStartRequest usageStartRequest) {
        UsagePcRoom usagePcRoom = new UsagePcRoom.UsagePcRoomStartBuilder(memberPcRoom, usageStartRequest).build();
        usagePcRoomRepository.save(usagePcRoom);
    }

    // PC방 회원 정보 조회
    public MemberPcRoomDetail getUsagePcRoom(String loginId) {
        MemberPcRoom memberPcRoom = memberPcRoomRepository.findAllByLoginIdOrderByMemberNameAsc(loginId);
        return new MemberPcRoomDetail.MemberPcRoomDetailBuilder(memberPcRoom).build();
    }

    // PC방 회원 정보 전체 조회
    public ListResult<MemberPcRoomItem> getMemberPcRooms() {
        List<MemberPcRoom> memberPcRooms = memberPcRoomRepository.findAll();
        List<MemberPcRoomItem> result = new LinkedList<>();

        memberPcRooms.forEach(memberPcRoom -> {
            MemberPcRoomItem addItem = new MemberPcRoomItem.MemberPcRoomItemBuilder(memberPcRoom).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    // PC방 종료시간 수정(log-out)
    public void putUsagePcRoomEnd(String loginId) {
        UsagePcRoom usagePcRoom = usagePcRoomRepository.findAllByMemberPcRoom_LoginIdOrderByUsagePcRoomIdAsc(loginId);
        usagePcRoom.putUsagePcRoomEndData();
        usagePcRoomRepository.save(usagePcRoom);
    }

    // PC방 이용시간금액 전체조회
    public ListResult<UsagePcRoomItem> getUsagePrices() {
        List<UsagePcRoom> usagePcRooms = usagePcRoomRepository.findAll();
        List<UsagePcRoomItem> result = new LinkedList<>();

        usagePcRooms.forEach(usagePcRoom -> {
            UsagePcRoomItem addItem = new UsagePcRoomItem.UsagePcRoomItemBuilder(usagePcRoom).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
