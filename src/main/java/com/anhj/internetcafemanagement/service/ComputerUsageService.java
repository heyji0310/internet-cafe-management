package com.anhj.internetcafemanagement.service;

import com.anhj.internetcafemanagement.entity.Computer;
import com.anhj.internetcafemanagement.entity.OrderPaper;
import com.anhj.internetcafemanagement.entity.UsagePcRoom;
import com.anhj.internetcafemanagement.exception.CMissingDataException;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.computerModel.ComputerItem;
import com.anhj.internetcafemanagement.model.computerModel.ComputerRequest;
import com.anhj.internetcafemanagement.repository.ComputerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComputerUsageService {
    private final ComputerRepository computerRepository;

    public Computer getComputerData(long computerId) {
        return computerRepository.findById(computerId).orElseThrow(CMissingDataException::new);
    }
    public void setComputerUsage(UsagePcRoom usagePcRoom, OrderPaper orderPaper, ComputerRequest request) {
        Computer computer = new Computer.ComputerBuilder(usagePcRoom, orderPaper, request).build();
        computerRepository.save(computer);
    }

    public ComputerItem getComputerUsage(String seatNumber) {
        Computer computer = computerRepository.findAllBySeatNumberOrderBySeatNumberAsc(seatNumber);
        return new ComputerItem.ComputerItemBuilder(computer).build();
    }

    public ListResult<ComputerItem> getComputerUsages() {
        List<Computer> computers = computerRepository.findAll();
        List<ComputerItem> result = new LinkedList<>();

        computers.forEach(computer -> {
            ComputerItem addItem = new ComputerItem.ComputerItemBuilder(computer).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
