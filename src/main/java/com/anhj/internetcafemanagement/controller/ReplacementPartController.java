package com.anhj.internetcafemanagement.controller;

import com.anhj.internetcafemanagement.entity.Computer;
import com.anhj.internetcafemanagement.model.CommonResult;
import com.anhj.internetcafemanagement.model.SingleResult;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartDetail;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartItemResponse;
import com.anhj.internetcafemanagement.model.replacementPartModel.ReplacementPartRequest;
import com.anhj.internetcafemanagement.service.ComputerUsageService;
import com.anhj.internetcafemanagement.service.ReplacementPartService;
import com.anhj.internetcafemanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "부품 교체내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/replacement-part")
public class ReplacementPartController {
    private final ReplacementPartService replacementPartService;
    private final ComputerUsageService computerUsageService;

    @ApiOperation(value = "부품 교체내역 등록")
    @PostMapping("/part/new")
    public CommonResult setReplacementPart(@RequestBody @Valid ReplacementPartRequest request) {
        Computer computer = computerUsageService.getComputerData(request.getComputerId());
        replacementPartService.setReplacementPart(computer, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "부품 교체내역 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "replacementId", value = "부품교체내역 시퀀스", required = true)
    )
    @GetMapping("/replacement/{replacementId}")
    public SingleResult<ReplacementPartDetail> getReplacement(@PathVariable long replacementId) {
        return ResponseService.getSingleResult(replacementPartService.getReplacement(replacementId));
    }

    @ApiOperation(value = "PC 정보 상세조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "컴퓨터 시퀀스", required = true)
    )
    @GetMapping("/part-detail/{id}")
    public SingleResult<ReplacementPartItemResponse> getResponse(@PathVariable long id) {
        return ResponseService.getSingleResult(replacementPartService.getResponse(id));
    }
}
