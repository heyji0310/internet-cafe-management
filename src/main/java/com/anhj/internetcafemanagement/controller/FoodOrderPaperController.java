package com.anhj.internetcafemanagement.controller;

import com.anhj.internetcafemanagement.entity.Food;
import com.anhj.internetcafemanagement.enums.Categorize;
import com.anhj.internetcafemanagement.model.CommonResult;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.foodModel.FoodItem;
import com.anhj.internetcafemanagement.model.foodModel.FoodRequest;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperItem;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperRequest;
import com.anhj.internetcafemanagement.model.orderPaperModel.OrderPaperStatusUpdateRequest;
import com.anhj.internetcafemanagement.service.FoodOrderPaperService;
import com.anhj.internetcafemanagement.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "음식주문내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/food-order-paper")
public class FoodOrderPaperController {
    private final FoodOrderPaperService foodOrderPaperService;

    @ApiOperation(value = "음식메뉴 등록")
    @PostMapping("/food/new")
    public CommonResult setFood (@RequestBody @Valid FoodRequest foodRequest) {
        foodOrderPaperService.setFood(foodRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "주문내역 등록")
    @PostMapping("/order-paper/new")
    public CommonResult setOrderPaper (@RequestBody @Valid OrderPaperRequest orderPaperRequest) {
        Food food = foodOrderPaperService.getFoodData(orderPaperRequest.getFoodId());
        foodOrderPaperService.setOrderPaper(food, orderPaperRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "음식 재료소진으로 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "foodId", value = "음식 시퀀스", required = true)
    )
    @PutMapping("/food-false/{foodId}")
    public CommonResult putFoodIsOnSaleUpdateFalse(@PathVariable long foodId) {
        foodOrderPaperService.putFoodIsOnSaleUpdateFalse(foodId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "음식 주문가능으로 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "foodId", value = "음식 시퀀스", required = true)
    )
    @PutMapping("/food-true/{foodId}")
    public CommonResult putFoodIsOnSaleUpdateTrue(@PathVariable long foodId) {
        foodOrderPaperService.putFoodIsOnSaleUpdateTrue(foodId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "카테고리별 주문가능 음식메뉴 조회")
    @GetMapping("/food/search")
    public ListResult<FoodItem> getFoods(@RequestParam(value = "categorize", required = false) Categorize categorize) {
        if (categorize == null) {
            return ResponseService.getListResult(foodOrderPaperService.getFoods(), true);
        } else {
            return ResponseService.getListResult(foodOrderPaperService.getFoods(categorize), true);
        }
    }

    @ApiOperation(value = "음식주문내역 전체조회")
    @GetMapping("/order-paper/all")
    public ListResult<OrderPaperItem> getOrderPapers() {
        return ResponseService.getListResult(foodOrderPaperService.getOrderPapers(), true);
    }

    @ApiOperation(value = "음식주문상태 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "orderPaperId", value = "음식주문내역 시퀀스", required = true)
    )
    @PutMapping("/order-paper/{orderPaperId}")
    public CommonResult putOrderPaperStatus(long orderPaperId, OrderPaperStatusUpdateRequest statusUpdateRequest) {
        foodOrderPaperService.putOrderPaperStatus(orderPaperId, statusUpdateRequest);
        return ResponseService.getSuccessResult();
    }
}
