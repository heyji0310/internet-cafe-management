package com.anhj.internetcafemanagement.controller;

import com.anhj.internetcafemanagement.entity.MemberPcRoom;
import com.anhj.internetcafemanagement.model.CommonResult;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.SingleResult;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomDetail;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomItem;
import com.anhj.internetcafemanagement.model.memberPcRoomModel.MemberPcRoomRequest;
import com.anhj.internetcafemanagement.model.usagePcRoomModel.UsagePcRoomItem;
import com.anhj.internetcafemanagement.model.usagePcRoomModel.UsagePcRoomStartRequest;
import com.anhj.internetcafemanagement.service.PcRoomMemberUsageService;
import com.anhj.internetcafemanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-usage")
public class MemberPcRoomController {
    private final PcRoomMemberUsageService pcRoomMemberUsageService;

    @ApiOperation(value = "회원정보 등록")
    @PostMapping("/member/new")
    public CommonResult setMemberPcRoom(@RequestBody @Valid MemberPcRoomRequest memberRequest) {
        pcRoomMemberUsageService.setMemberPcRoom(memberRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 로그인")
    @PostMapping("/usage-log-in/new")
    public CommonResult setUsagePcRoomStart(@RequestBody @Valid UsagePcRoomStartRequest usageStartRequest) {
        MemberPcRoom memberPcRoom = pcRoomMemberUsageService.getMemberData(usageStartRequest.getMemberId());
        pcRoomMemberUsageService.setUsagePcRoomStart(memberPcRoom, usageStartRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 아이디 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "loginId", value = "회원 로그인 아이디", required = true)
    )
    @GetMapping("/member/{loginId}")
    public SingleResult<MemberPcRoomDetail> getUsagePcRoom(@PathVariable @Valid String loginId) {
        return ResponseService.getSingleResult(pcRoomMemberUsageService.getUsagePcRoom(loginId));
    }

    @ApiOperation(value = "회원 정보 전체조회")
    @GetMapping("/member/all")
    public ListResult<MemberPcRoomItem> getMemberPcRooms() {
        return ResponseService.getListResult(pcRoomMemberUsageService.getMemberPcRooms(), true);
    }

    @ApiOperation(value = "회원 로그아웃")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "loginId", value = "회원 로그인 아이디", required = true)
    )
    @PutMapping("/usage-log-out/{loginId}")
    public CommonResult putUsagePcRoomEnd(@PathVariable @Valid String loginId) {
        pcRoomMemberUsageService.putUsagePcRoomEnd(loginId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "PC방 이용시간금액 전체조회")
    @GetMapping("/usage/price/all")
    public ListResult<UsagePcRoomItem> getUsagePrices() {
        return ResponseService.getListResult(pcRoomMemberUsageService.getUsagePrices(), true);
    }
}
