package com.anhj.internetcafemanagement.controller;

import com.anhj.internetcafemanagement.entity.OrderPaper;
import com.anhj.internetcafemanagement.entity.UsagePcRoom;
import com.anhj.internetcafemanagement.model.CommonResult;
import com.anhj.internetcafemanagement.model.ListResult;
import com.anhj.internetcafemanagement.model.SingleResult;
import com.anhj.internetcafemanagement.model.computerModel.ComputerItem;
import com.anhj.internetcafemanagement.model.computerModel.ComputerRequest;
import com.anhj.internetcafemanagement.service.ComputerUsageService;
import com.anhj.internetcafemanagement.service.FoodOrderPaperService;
import com.anhj.internetcafemanagement.service.PcRoomMemberUsageService;
import com.anhj.internetcafemanagement.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "컴퓨터 사용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/computer-usage")
public class ComputerUsageController {
    private final ComputerUsageService computerUsageService;
    private final PcRoomMemberUsageService pcRoomMemberUsageService;
    private final FoodOrderPaperService foodOrderPaperService;

    @ApiOperation(value = "컴퓨터 이용내역 등록")
    @PostMapping("/usage/new")
    public CommonResult setComputerUsage(@RequestBody @Valid ComputerRequest request) {
        UsagePcRoom usagePcRoom = pcRoomMemberUsageService.getUsagePcRoomData(request.getUsagePcRoomId());
        OrderPaper orderPaper = foodOrderPaperService.getOrderPaperData(request.getOrderPaperId());
        computerUsageService.setComputerUsage(usagePcRoom, orderPaper, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "컴퓨터 이용내역 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "seatNumber", value = "좌석번호", required = true)
    )
    @GetMapping("/member/{seatNumber}")
    public SingleResult<ComputerItem> getComputerUsage(@PathVariable @Valid String seatNumber) {
        return ResponseService.getSingleResult(computerUsageService.getComputerUsage(seatNumber));
    }

    @ApiOperation(value = "컴퓨터 이용내역 전체조회")
    @GetMapping("/usage/all")
    public ListResult<ComputerItem> getComputerUsages() {
        return ResponseService.getListResult(computerUsageService.getComputerUsages(), true);
    }
}
