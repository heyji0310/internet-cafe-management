# PC방 관리 프로그램

<img src="https://files.picaplay.com/upload/93/95/thumb2_acb1fa5dd23d4fd9b16091a2e6f9a219.jpg" width="480px" height="270px" title="px(픽셀) 크기 설정" alt="피씨방 관리"></img><br/>


### version
```
0.0.1
```


#### Language
```
JAVA 16
springBoot 2.7.3
```

***

#### 기능
* PC방 관리

|       __기능__       |  __기능명__ |
|:------------------:|:--------:|
|      Computer      |  PC 컴퓨터  |
|    Order Sheet     |  음식주문내역  |
|        Food        |  음식 메뉴   |
|      PC Usage      | PC 사용내역  |
|   PC Room Member   |  PC방 회원  |
|  Replacement Part  | 부품 교체내역  |


***